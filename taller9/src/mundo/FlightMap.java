package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import estructuras.MapEdge;
import estructuras.MapGraph;
import estructuras.MapNode;

public class FlightMap {

	/**
	 * Archivo que contiene la información de los nodos que corresponden a los
	 * aeropuertos del mundo.
	 */
	private final static String NODES_FILE = "./data/nodes.csv";

	/**
	 * Archivo que contiene la información de las aerolíneas.
	 */
	private final static String NAMES_FILE = "./data/names.csv";

	/**
	 * Archivo que contiene la información de los arcos que corresponden a los vuelos
	 * existentes entre los aeropuertos.
	 */
	private final static String EDGES_FILE = "./data/edges.csv";

	/**
	 * Grafo correspondiente a los vuelos, aerolíneas y aeropuertos.
	 */
	private MapGraph graph;

	private Map<Integer, String> airlines;


	/**
	 * Constructor principal de la clase, esta se encuentra encargada de realizar la
	 * inicialización y carga de los archivos de descripción de la representación de
	 * grafo de los aeropuertos, vuelos y aeroíneas.
	 */
	public FlightMap()
	{
		//TODO inicialice graph y airlines

		//Nodos
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(NODES_FILE));
			String line;
			br.readLine();
			while((line = br.readLine()) != null)
			{
				try{
					String[] values = line.split(",");
					//TODO aguregue un nuevo nodo al grafo utilizando la información de values. Recuerde leer el enunciado
					//para saber que posicion necesita. 
					//Tip: Necesita los valores de ID, latitud, longitud, nombre, ciudad, pais, codigo IATA (nombreConvenvional)
				}catch(Exception e){
					//Algunos valores en el archivo de entrada tienen defectos, si desea manejarlos acá
					//como bono, puede hacerlo. De lo contrario deje este espacio en blanco.
				}

			}
			br.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al leer el archivo de nodos: " + e.getMessage());
		}

		//Arcos
		try
		{
			br = new BufferedReader(new FileReader(EDGES_FILE));
			String line;
			br.readLine();
			while((line = br.readLine()) != null)
			{        
				try{
					String[] values = line.split(",");
					//TODO aguregue un nuevo arco al grafo utilizando la información de values. Recuerde leer el enunciado
					//para saber que posicion necesita. 
					//Tip: Necesita los valores de ID1, ID2, ID_Aerolinea, costo
				}catch(Exception e){
					//Algunos valores en el archivo de entrada tienen defectos, si desea manejarlos acá
					//como bono, puede hacerlo. De lo contrario deje este espacio en blanco.
				}
			}
			br.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al leer el archivo de arcos: " + e.getMessage());
		}

		//Airlines
		try
		{
			br = new BufferedReader(new FileReader(NAMES_FILE));
			String line;
			br.readLine();
			while((line = br.readLine()) != null)
			{        
				try{
					String[] values = line.split(",");
					//TODO aguregue un una entrada de aerolinea utilizando values. Recuerde leer el enunciado
					//para saber que posicion necesita. 
					//Tip: Necesita los valores de ID, nombre
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			br.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al leer el archivo de nombres: " + e.getMessage());
		}
	}

	/**
	 * P1) Busca un Aeropuerto dado un id
	 * @param id - El id del Aeropuerto
	 * @return El Aeropuerto buscado, null si no existe.
	 */
	public MapNode getNodeById(int id){
		//TODO Devuelva el nodo dado un id
		return null;
	}

	/**
	 * P2) Busca un Aeropuerto dado su identificador IATA
	 * @param IATA - El código IATA del Aeropuerto
	 * @return El Aeropuerto buscado, null si no existe.
	 */
	public MapNode getNodeByIATA(String IATA){
		//TODO Devuelva el nodo dado un codigo IATA
		return null;
	}

	/**
	 * P3) Devuelve los códigos IATA de todos los aeropuertos del mapa.
	 * @return Una lista con todos los códigos encontrados.
	 */
	public List<String> getAllIATACodes(){
		//TODO Devuelva una lista de Strings que contenga todos los codigos
		//IATA del grafo.

		return null;
	}

	/**
	 * BONO) Busca un vuelo entre dos Aeropuertos dados sus id's
	 * @param from - id del Aeropuerto de origen
	 * @param to - id del Aeropuerto de destino
	 * @return El vuelo entre los aeropuertos, null si no existe.
	 */
	public String getFlight(int from, int to ){

		Set<MapEdge> edges = graph.getNodeEdges(from);
		if (edges == null)
			return null;
		for (MapEdge edge : edges){
			if (edge.to == to){
				return edge.toString(airlines.get(edge.airline));
			}
		}
		return null;
	}

}


