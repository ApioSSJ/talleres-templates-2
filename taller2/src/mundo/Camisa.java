package mundo;

public class Camisa extends Vestuario{
	
	public enum Marca
	{
		GEF, POLO, NAUTICA
	}
	
	public static final String MARCA_GEF = "GEF";
	public static final String MARCA_POLO = "POLO";
	public static final String MARCA_NAUTICA = "NAUTICA";

	
	private Marca marca;
	
	public Camisa(Marca pMarca, Talla pTalla, double pPrecio)
	{
		super(pTalla, pPrecio);
		marca = pMarca;
	}
	
	public String toString()
	{
		return "Camisa "+marca.name() + " Talla "+talla + "($ "+precio + ")";
	}

}
