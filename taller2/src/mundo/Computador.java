package mundo;

public class Computador extends Electronico{

	public enum MarcasComputador
	{
		HP, DELL, LENOVO, TOSHIBA, APPLE
	}
	
	private MarcasComputador marca;
	
	public Computador(Gama pGama, double pPrecio, MarcasComputador pMarca)
	{
		super(pGama, pPrecio);
		marca = pMarca;
	}
	
	public String toString()
	{
		return "Computador "+marca.name()+" - "+"Gama "+gama + "($ "+precio + ")";
	}
}
